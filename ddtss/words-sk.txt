KDE Office Suite	Kancelársky balík KDE
KDE PIM	PIM KDE
KDE SC utilities	Nástroje KDE
KDE Software Development Kit	SDK KDE
KDE accessibility	Prístupnosť KDE
KDE base workspace	Základný pracovný priestor KDE
KDE education	Vzdelávanie KDE
KDE games	Hry KDE
KDE multimedia	Multimédiá KDE
KDE toys	Hračky KDE
KDE web development	Vývoj webu KDE
access	prístup
access control	riadenie prístupu
access control list	pravidlá riadenia prístupu
access-control	riadenie prístupu
access-control-list	pravidlá riadenia prístupu
accesscontrol	riadenie prístupu
accesscontrollist	pravidlá riadenia prístupu
administration	správa
administrator	správca
affine transformation	afínna transformácia
affine-transformation	afínna transformácia
affinetransformation	afínna transformácia
album art	obal albumu
anti aliased	antialiasované
anti-aliased	antialiasované
antialiased	antialiasované
antialiasing	antialiasing
applet	aplet
assembly	zostavenie (.NET)
authentication	overenie totožnosti
authenticity	pravosť, totožnosť
bezier	Bézierov
bezier curve	Bézierova krivka
bézier	Bézierov
bézier curve	Bézierova krivka
bi-directional	obojsmerný (komunikácia), dvojsmerný (text)
bidirectional	obojsmerný (komunikácia), dvojsmerný (text)
boot	zavedenie systému, zaviesť systém
booting	zavedenie systému
box	pole
bug	chyba
bug tracking system	systém na sledovanie chýb
bug-tracking-system	systém na sledovanie chýb
bugtrackingsystem	systém na sledovanie chýb
build	zostavenie
built	zostavený
button	tlacidlo
canvas	plátno
certificate authorities	certifikačné autority
certificate authority	certifikačná autorita
charset	znaková sada
codec	kodek
collection	kolekcia
command line	príkazový riadok
command-line	príkazový riadok
commandline	príkazový riadok
common	spoločný
compatibility	kompatibilita
compile	kompilovať, skompilovaný
completion	dopĺňanie
component	komponent
compress	komprimovať
compressed	komprimovaný
compression	komprimácia
console	konzola
constitution	ústava
control	riadiť, riadenie
controller	kontrolér
core	základný, hlavný
cross platform	multiplatformný, multiplatformový
cross-platform	multiplatformný, multiplatformový
crossplatform	multiplatformný, multiplatformový
daemon	démon
debian installer	inštalátor Debianu
debian-installer	inštalátor Debianu
debianinstaller	inštalátor Debianu
debug	ladiť
debugging	ladenie
debugging symbols	ladiace symboly
debugging-symbols	ladiace symboly
debuggingsymbols	ladiace symboly
decoder	dekodér (neživ.), dekódovač (živ.)
decompress	dekomprimovať
decompressed	dekomprimovaný
decompression	dekomprimácia
decryption	dešifrovanie
default	štandardný, predvolený
demultiplexor	demultiplexor
demuxer	demultiplexor
deprecated	zavrhovaný
desktop	pracovné prostredie
desktop	pracovné prostredie, pracovná plocha
desktop environment	pracovné prostredie
detect	detekovať
develop	vyvíjať
development	vývoj
development environment	vývojové prostredie
development files	súbory na vývoj
development-files	súbory na vývoj
developmentfiles	súbory na vývoj
directory	adresár
disable	vypnúť, zakázať, znemožniť
disabled	vypnuté, zakázané; postihnutí
domain	doména
domain name	doménový názov
domain-name	doménový názov
domainname	doménový názov
driver	ovládač
drivers	ovládače
dummy	fiktívny, prechodný (balík)
efficiency	výkonnosť
efficient	výkonný
embed	vkladať
embedded	vnorený
emoticon	emotikon (neživ., vzor dub)
enable	zapnúť, povoliť, umožniť
encoding	kódovanie
encoder	kodér (neživ.), kódovač (živ.)
encryption	šifrovanie
engine	jadro
entry	záznam; vstup
entries	záznamy
extension	rozšírenie
fail	zlyhať, nepodariť sa, nebyť úspešný
failure	zlyhanie
feature	vlastnosť, schopnosť
fill	výplň
firewall	firewall (vzor dub)
firmware	firmvér
flavor	verzia
folder	priečinok
font	písmo
fonts	písma
footprint	odtlačok
formidable	impozantný, úctyhodný
formula	vzorec
free software	slobodný softvér
free software guidelines	určenie slobodného softvéru
free-software	slobodný softvér
free-software-guidelines	určenie slobodného softvéru
freesoftware	slobodný softvér
freesoftwareguidelines	určenie slobodného softvéru
full screen	celoobrazovkový
full screen mode	celoobrazovkový režim
hardware	hardvér
hash	haš
header files	hlavičkové súbory
header-files	hlavičkové súbory
headerfiles	hlavičkové súbory
headers	hlavičkové súbory
high quality	kvalitný
high-quality	kvalitný
highquality	kvalitný
hinting	dolaďovanie písma, optimalizácia písma
home page	domovská stránka
home-page	domovská stránka
homepage	domovská stránka
host	hostiteľ
host computer	hostiteľský počítač
host machine	hostiteľský počítač
hosting	hosting
ICC	profil farieb ICC
IDE	IDE (integrované vývojové prostredie)
installer	inštalátor
instance	inštancia
integration	integrácia
integrated development environment	integrované vývojové prostredie
intended	cielený
interactive	interaktívny
interface	rozhranie
internationalization	internacionalizácia
internationalized	internacionalizovaný
ISV	nezávislý dodávateľ softvéru
key	kláves; kľúč
keyboard shortcut	klávesová skratka
keyboard shortcuts	klávesové skratky
keyboard-shortcut	klávesová skratka
keyboard-shortcuts	klávesové skratky
keyboardshortcut	klávesová skratka
keyboardshortcuts	klávesové skratky
legacy	spätná kompatibilita
library	knižnica
lightweight	odľahčený
linked	spojený s
localization	lokalizácia
localized	lokalizovaný
log	záznam
logging	zaznamenávanie
lossless	bezstratový
lossy	stratový
lookup	vyhľadať
mailing list	konferencia
mailing-list	konferencia
mailinglist	konferencia
main	hlavný
manage	spravovať
management	správa
manager	správca
manual	príručka k XXX/príručka na používanie XXX
markov	Markovov
markov model	Markovov model
markov chain	Markovov reťazec
media	multimédiá
merging	zlučovanie, spájanie
metadata	metadáta
mirror	zrkadlo
mode	režim
mount point	miesto pripojenia; bod pripojenia
mount-point	miesto pripojenia; bod pripojenia
mountpoint	miesto pripojenia; bod pripojenia
multi platform	multiplatformový
multi-platform	multiplatformový
multiplatform	multiplatformový
multiplexor	multiplexor
muxer	multiplexor
notification area	oznamovacia oblasť
notification-area	oznamovacia oblasť
notificationarea	oznamovacia oblasť
obsolete	zastaraný
office suite	kancelársky balík
office-suite	kancelársky balík
officesuite	kancelársky balík
opacity	krytie
open source	open source
open-source	open source
opensource	open source
optional	nepovinný, voliteľný
package	balík
packet	paket
packetization	paketizácia
packetizer	paketizér, paketizovač
patch	záplata
pane	panel
paragraph	odstavec
permissions	oprávnenia, prístupové práva
piece	(herná) figúra
playlist	zoznam stôp
plugin	zásuvný modul
pool	fond
portable	portabilný
power user	skúsený používateľ, pokročilý používateľ
productivity	produktivita; kancelárske nástroje, kancelársky balík
provide	poskytovať
provided	poskytovaný
provides	poskytuje
puzzle game	logická hra
query	požiadavka
recommend	odporúčať
recommended	odporúčaný
recommends	odporúča
relation	vzťah
render	vykresľovať
rendering	vykresľovanie
repo	zdroj softvéru (Debian), úložisko (VCS)
repository	zdroj softvéru (Debian), úložisko (VCS)
request	požiadavka
require	požadovať
required	požadovaný
requires	požaduje
resource	zdroj, prostriedok
router	smerovač
routine	funkcia
routines	funkcie
royalty-free	bezplatný
scalable	škálovateľný
scale	zmenia mierky; škála
screenshot	snímka obrazovky
session	relácia
set	sada, množina
showcase	demonštrovať
smart	inteligentný
smiley	smajlík (živ./neživ., vzor chlap), smejko (živ., vzor chlap) 
snapshot	snímka
snippet	úryvok
social contract	Spoločenská zmluva
social-contract	Spoločenská zmluva
socialcontract	Spoločenská zmluva
software	softvér
software vendor	dodávateľ softvéru
sophisticated	prepracovaný, premyslený, dômyselný, rafinovaný
standalone	samostatný
standard	štandard; štandardný
stream	dátový tok, streamovať
streamer	streamer
streamed	streamovaný
streaming	streaming, streamovanie 
stroke	ťah
suggest	navrhovať
suggested	navrhovaný
suggests	navrhuje
support	podpora
supported	podporovaný
supports	podporuje
swap	odkladanie; odkladacia oblasť
swap file	odkladací súbor
swap-file	odkladací súbor
swapfile	odkladací súbor
swapping	odkladanie
switch	prepínač (sieť.)
symbolic link	symbolický odkaz
symlink	symbolický odkaz, vytvoriť symbolický odkaz
syntax highlighting	zvýrazňovanie syntaxe
system administrator	správca systému
system tray	oznamovacia oblasť
system-tray	oznamovacia oblasť
systemtray	oznamovacia oblasť
systray	oznamovacia oblasť
tab	karta
tag	značka
tags	značky
team	tím
temporal	dočasný, prechodný
temporary	dočasný, prechodný
third party	(softvér) tretích strán
thread	vlákno
threaded	využívajúci vlákna
tool	nástroj
tools	nástroje
touchpad	dotyková podložka
transitional	prechodový
transcoder	transkodér
transcoding	transkódovanie
translucent	priesvitný
trusted	dôverovaný
trustworthy	dôverovaný
trustworthiness	dôveryhodný
typography	typografia
ubiquitous	všadeprítomný
ubiquity	všadeprítomnosť
unix like	unixový
unix-like	unixový
unixlike	unixový
upstream	upstream
usage	použitie; využitie
user	používateľ
user experience	používateľský zážitok
user friendly	používateľsky prívetivý
user interface	používateľské rozhranie
user-friendly	používateľsky prívetivý
user-interface	používateľské rozhranie
user interface	používateľské rozhranie
user space	používateľský priestor
utilities	nástroje
utility	nástroj
variant	variant
vector based	vektorový
vector graphics	vektorová grafika
vector-based	vektorový
vector-graphics	vektorová grafika
vectorbased	vektorový
vectorgraphics	vektorová grafika
viewer	prehliadač
volunteer	dobrovoľník
warning	upozornenie
web server	webový server
web-server	webový server
webserver	webový server
widget	ovládací prvok
workgroup	pracovná skupina
working group	pracovná skupina
working-group	pracovná skupina
workinggroup	pracovná skupina
