account		учётная запись
application	программа
backend	движок, драйвер
boot floppy	загрузочная дискета
boot loader	системный загрузчик
bootstrap partition	загрузочный раздел
browser	браузер
default	по умолчанию
cache	кэш
CD		компакт-диск
CD-ROM		компакт-диск, привод CD-ROM
configuration	настройка, конфигурация
configure	настраивать
customize	настраивать
directory	каталог
download	загрузить
dummy	пустой
feature	возможность, особенность, опция
firmware	микропрограмма
floppy disk	дискета
frontend	оболочка, интерфейс
hard link	жёсткая ссылка
hardware	аппаратура, аппаратное обеспечение
host	компьютер, узел
hostname	имя компьютера
install	установить
installation	установка
installer	программа установки
Internet	интернет
IP address	IP-адрес
kernel	ядро
kernel image	образ ядра
kernel package	пакет с ядром
keymap	раскладка клавиатуры
login	имя пользователя, логин
machine	компьютер, машина
maintainer	сопровождающий, маинтейнер
master boot record	основная загрузочная запись
mirror	зеркало
mount	монтировать, смонтировать
mounted	смонтированный
mount point	точка монтирования
netmask	маска подсети
partition	раздел
pin	фиксация, фиксировать, фиксатор (пакета)
reboot	перезагрузка, перезагрузить
reinstall	переустановить
repository	репозитарий
resolution	разрешение
resolve	разрешить
retriever	загрузчик компонентов программы установки
root partition	корневой раздел
runtime library		динамическая библиотека
socket	сокет
software	программа, программное обеспечение
superuser	суперпользователь
swap file	файл подкачки
swap partition	раздел подкачки
symbolic link	символьная ссылка
tag	метка
time zone	часовой пояс
unmount	размонтировать
