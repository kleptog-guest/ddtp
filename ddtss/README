DDTSS - The Debian Distributed Translation Server Satellite

I wrote the script basically so I could to the translation stuff with the
DDTS without actually having to handle all the email stuff myself. This
basically handles the communication via email and presents a web interface
to the user.

It can send and receive emails from the DDTS and provides a web interface
where you can do the actual translating. It has the capability to let people
review translations before sending them. Reviewers can also amend the
translations straight away.

It's a bit rough, but I'm using it for my own translation submissions. It
consists of four main programs. For more installation notes, see the end of
the file.

ddtss-setup
-----------

This creates and configures the initial setup. Edit this script to configure
the language as well as your own email address. It also allows you to
configure the number of reviewers needed and the number of untranslated
descriptions to keep around.

You have to run this first to create the database file, nothing else will do
that for you.

ddtss-cgi
---------

This is the actual CGI script that talks to the user. You need to make a
symlink to it from somewhere the webserver can see it. 

ddtss-receive
-------------

This is the script that processes incoming mail. The intention is that you
run it from your procmailrc like so:

:0c
* From: pdesc@ddtp.debian.net
| ~/perl/wherever/ddtss-receive

However, for testing purposes you can just pipe the emails from your mailer
into the script.

ddtss-process
-------------

This script does various background processing. The intention is that this
is run from cron on a regular basis. Amongst other things it does:

- Processes the things received by ddtss-receive
- Send off translations with the requisite number of reviewers
- Fetch new translations if the number remaining falls below the minimum

Other files are:

DDTSS.pm    - module to handle some of the common stuff
README      - this file
test.pl     - dumps the DB file so you can see what's going on

INSTALLATION NOTES
==================

Other than the Apache configuration and procmail setup described above,
there are a few other tweaks needed to get it to work. For example, you need
to tell the CGI script and the receive and process scripts where the data
file is. In all these files there is a line as follows:

use lib '/home/kleptog/perl/ddtss';

Change this to your installation directory. Also, there is a line at the top
of DDTSS.pm which needs to be updated.

Additionally, the following Perl modules are required (debian package name
provided):

DB_File::Lock                libdb-file-lock-perl
Mail::Sender                 libmail-sender-perl
CGI                          (standard debian perl module)

MISC
====

Written by Martijn van Oosterhout <kleptog@svana.org> Copyright Aug 2006
The code is released freely under the GPL v2+.

