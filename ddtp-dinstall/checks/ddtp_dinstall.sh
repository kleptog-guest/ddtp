#!/bin/bash -eu
# 
# $Id$
#
# Copyright (C) 2008, Felipe Augusto van de Wiel <faw@funlabs.org>
# Copyright (C) 2008, Nicolas François <nicolas.francois@centraliens.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2

TIMESTAMP=$(date +%Y%m%d_%H%M%S)

DDTP_I18N_CHECK=$(dirname $(readlink -f $0))/ddtp_i18n_check.sh

TRANSLATION_FILES=/srv/ddtp.debian.net/Translation-files
PACKAGE_LISTS=/srv/ddtp.debian.net/packagelist
DDTP_DINSTALL_DIR=/srv/ddtp-dinstall

CHECK_DIR="$DDTP_DINSTALL_DIR/tf.check"
CHECK_PACKAGE_LISTS="$DDTP_DINSTALL_DIR/pkg-lists.check"
OLD_TARGET=$(readlink "$DDTP_DINSTALL_DIR/to-dak")
TMP_LINK="$DDTP_DINSTALL_DIR/to-dak-tmp"
DAK_LINK="$DDTP_DINSTALL_DIR/to-dak"

for d in "$TRANSLATION_FILES" "$PACKAGE_LISTS"; do
	if [ ! -d "$d" ]; then
		echo "Invalid directory '$d'"
		exit 1
	fi
done

# A new process :-)
cd "$DDTP_DINSTALL_DIR"

# Clean the check directory to prepare a new one
rm -rf "$CHECK_DIR" "$CHECK_PACKAGE_LISTS"
cp -a "$TRANSLATION_FILES" "$CHECK_DIR"
cp -a "$PACKAGE_LISTS" "$CHECK_PACKAGE_LISTS"
$DDTP_I18N_CHECK "$CHECK_DIR" "$PACKAGE_LISTS"
rm -rf "$CHECK_DIR" "$CHECK_PACKAGE_LISTS"

# Copy the new Translation files and create the new symlink
cp -a "$TRANSLATION_FILES" "$DDTP_DINSTALL_DIR/tf.$TIMESTAMP"
ln -s "$DDTP_DINSTALL_DIR/tf.$TIMESTAMP" "$TMP_LINK"

# Atomic change the files seen by dak
mv -fT "$TMP_LINK" "$DAK_LINK"

# Only try to removes $OLD_TARGET if it exists
if [ -d "$OLD_TARGET" ]; then
	rm -rf "$OLD_TARGET"
fi

